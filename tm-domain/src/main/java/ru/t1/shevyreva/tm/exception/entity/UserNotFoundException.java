package ru.t1.shevyreva.tm.exception.entity;

public class UserNotFoundException extends AbstractEntityException {

    public UserNotFoundException() {
        super("Error! User not found.");
    }

}
