package ru.t1.shevyreva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.model.User;

@Setter
@Getter
@NoArgsConstructor
public class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@NotNull UserDTO user) {
        super(user);
    }

}
