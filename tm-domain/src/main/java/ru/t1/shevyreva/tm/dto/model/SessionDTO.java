package ru.t1.shevyreva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "tm_session")
@NoArgsConstructor
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column(name = "role", nullable = false)
    private Role role = null;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date date = new Date();

}
