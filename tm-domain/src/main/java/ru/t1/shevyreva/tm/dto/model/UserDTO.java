package ru.t1.shevyreva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.model.AbstractModel;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tm_user")
@NoArgsConstructor
public final class UserDTO extends AbstractModelDTO {

    @NotNull
    @Column(name = "login", nullable = false)
    private String login;

    @NotNull
    @Column(name = "password", nullable = false)
    private String passwordHash;

    @Nullable
    @Column(name = "email", nullable = true)
    private String email;

    @Nullable
    @Column(name = "first_name", nullable = true)
    private String firstName;

    @Nullable
    @Column(name = "last_name", nullable = true)
    private String lastName;

    @Nullable
    @Column(name = "middle_name", nullable = true)
    private String middleName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "locked", nullable = false)
    private Boolean locked = false;

}
