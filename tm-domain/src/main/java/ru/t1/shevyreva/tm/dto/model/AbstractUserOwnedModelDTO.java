package ru.t1.shevyreva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.AbstractModel;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnedModelDTO extends AbstractModel {

    @Nullable
    @Column(name = "user_id", nullable = true)
    protected String userId;

}
