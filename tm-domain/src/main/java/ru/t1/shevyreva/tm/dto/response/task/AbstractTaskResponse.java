package ru.t1.shevyreva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;
import ru.t1.shevyreva.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private TaskDTO task;

    public AbstractTaskResponse(@Nullable final TaskDTO task) {
        this.task = task;
    }

}
