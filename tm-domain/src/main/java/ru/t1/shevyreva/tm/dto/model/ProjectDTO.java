package ru.t1.shevyreva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.model.IWBS;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "tm_project")
@NoArgsConstructor
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    @NotNull
    @Column(name = "name", nullable = false)
    private String name = "";

    @NotNull
    @Column(name = "description", nullable = false)
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status = Status.NON_STARTED;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    public ProjectDTO(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

}
