package ru.t1.shevyreva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "tm_session")
@NoArgsConstructor
public final class Session extends AbstractUserOwnedModel {

    @NotNull
    @Column(name = "role", nullable = false)
    private Role role = null;

    @NotNull
    @Column(name = "created", nullable = false)
    private Date date = new Date();

}
