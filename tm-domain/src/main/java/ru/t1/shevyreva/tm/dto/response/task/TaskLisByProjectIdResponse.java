package ru.t1.shevyreva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class TaskLisByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<TaskDTO> tasks;

    public TaskLisByProjectIdResponse(@Nullable List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
