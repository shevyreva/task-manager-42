package ru.t1.shevyreva.tm;


import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.shevyreva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.shevyreva.tm.api.endpoint.IUserEndpoint;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.dto.request.user.*;
import ru.t1.shevyreva.tm.dto.response.user.*;
import ru.t1.shevyreva.tm.marker.SoapCategory;
import ru.t1.shevyreva.tm.model.User;
import ru.t1.shevyreva.tm.service.PropertyService;
import ru.t1.shevyreva.tm.util.HashUtil;

import java.util.UUID;

@Category(SoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private String token = null;

    @Before
    public void init() {
        @NotNull final UserLoginRequest request = new UserLoginRequest("test", "test");
        @NotNull final UserLoginResponse response = authEndpoint.login(request);
        Assert.assertNotNull(response);
        token = response.getToken();
    }

    @Test
    public void testRegistry() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(login, password, email);

        @NotNull final UserRegistryResponse response = userEndpoint.registry(request);
        @NotNull final UserDTO user = response.getUser();
        Assert.assertNotNull(response);
        Assert.assertNotNull(user);

        Assert.assertThrows(Exception.class, () ->
                userEndpoint.registry(null));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.registry(new UserRegistryRequest(null, password, email)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.registry(new UserRegistryRequest(login, null, email)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.registry(new UserRegistryRequest(login, password, null)));
    }

    @Test
    public void testChangePassword() {
        authEndpoint.logout(new UserLogoutRequest(token));

        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @NotNull final UserRegistryRequest createRequest = new UserRegistryRequest(login, password, email);
        userEndpoint.registry(createRequest);

        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(login, password);
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(loginRequest);
        @NotNull final String newToken = loginResponse.getToken();

        @NotNull final String newPassword = "test";
        @NotNull final String hashNewPassword = HashUtil.salt(newPassword, propertyService);
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(newToken, newPassword);

        @NotNull final UserChangePasswordResponse response = userEndpoint.changePassword(request);
        Assert.assertNotNull(response);
        Assert.assertEquals(hashNewPassword, response.getUser().getPasswordHash());

        Assert.assertThrows(Exception.class, () ->
                userEndpoint.changePassword(null));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.changePassword(new UserChangePasswordRequest(null, newPassword)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.changePassword(new UserChangePasswordRequest(newToken, null)));
    }

    @Test
    public void testUpdateProfile() {
        @NotNull final UserProfileResponse profileResponse = authEndpoint.profile(new UserProfileRequest(token));
        @NotNull final String oldFirstName = profileResponse.getUser().getFirstName();
        @NotNull final String oldMiddleName = profileResponse.getUser().getMiddleName();
        @NotNull final String oldLastName = profileResponse.getUser().getLastName();

        @NotNull final String firstName = UUID.randomUUID().toString();
        @NotNull final String lastName = UUID.randomUUID().toString();
        @NotNull final String middleName = UUID.randomUUID().toString();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(token, firstName, middleName, lastName);

        @NotNull final UserUpdateProfileResponse response = userEndpoint.updateProfile(request);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(oldFirstName, response.getUser().getFirstName());
        Assert.assertNotEquals(oldMiddleName, response.getUser().getMiddleName());
        Assert.assertNotEquals(oldLastName, response.getUser().getLastName());

        Assert.assertThrows(Exception.class, () ->
                userEndpoint.updateProfile(null));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.updateProfile(new UserUpdateProfileRequest(null, firstName, middleName, lastName)));
    }

    @Test
    public void testLockUser() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("admin", "admin");
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(loginRequest);
        final String newToken = loginResponse.getToken();

        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @NotNull final UserRegistryRequest createRequest = new UserRegistryRequest(login, password, email);
        userEndpoint.registry(createRequest);

        @NotNull final UserLockRequest request = new UserLockRequest(newToken, login);
        @NotNull final UserLockResponse response = userEndpoint.lockUser(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(response.getUser().getLocked());

        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockUser(null));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockUser(new UserLockRequest(null, login)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockUser(new UserLockRequest(newToken, null)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.lockUser(new UserLockRequest(token, login)));

        authEndpoint.logout(new UserLogoutRequest(newToken));
        authEndpoint.logout(new UserLogoutRequest(token));
    }

    @Test
    public void testUnlockUser() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("admin", "admin");
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(loginRequest);
        final String newToken = loginResponse.getToken();

        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @NotNull final UserRegistryRequest createRequest = new UserRegistryRequest(login, password, email);
        userEndpoint.registry(createRequest);

        @NotNull final UserUnlockRequest request = new UserUnlockRequest(newToken, login);

        @NotNull final UserUnlockResponse response = userEndpoint.unlockUser(request);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.getUser().getLocked());

        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockUser(null));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockUser(new UserUnlockRequest(null, login)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockUser(new UserUnlockRequest(newToken, null)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.unlockUser(new UserUnlockRequest(token, login)));

        authEndpoint.logout(new UserLogoutRequest(newToken));
        authEndpoint.logout(new UserLogoutRequest(token));
    }

    @Test
    public void testRemoveUserByLogin() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest("admin", "admin");
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(loginRequest);
        Assert.assertNotNull(loginResponse);
        final String newToken = loginResponse.getToken();

        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @NotNull final UserRegistryRequest createRequest = new UserRegistryRequest(login, password, email);
        userEndpoint.registry(createRequest);

        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeUser(null));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeUser(new UserRemoveRequest(null, null, null, null)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeUser(new UserRemoveRequest(newToken, null, null, null)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeUser(new UserRemoveRequest(null, login, null, null)));
        Assert.assertThrows(Exception.class, () ->
                userEndpoint.removeUser(new UserRemoveRequest(token, login, null, null)));

        @NotNull final UserRemoveRequest request = new UserRemoveRequest(newToken, login, null, null);
        @NotNull final UserRemoveResponse response = userEndpoint.removeUser(request);
        Assert.assertNotNull(response);
        Assert.assertThrows(Exception.class, () ->
                authEndpoint.login(new UserLoginRequest(login, password)));

        authEndpoint.logout(new UserLogoutRequest(token));
        authEndpoint.logout(new UserLogoutRequest(newToken));
    }

}
