package ru.t1.shevyreva.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.shevyreva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.shevyreva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;
import ru.t1.shevyreva.tm.dto.request.project.*;
import ru.t1.shevyreva.tm.dto.request.user.UserLoginRequest;
import ru.t1.shevyreva.tm.dto.response.project.*;
import ru.t1.shevyreva.tm.dto.response.user.UserLoginResponse;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.marker.SoapCategory;
import ru.t1.shevyreva.tm.model.Project;

import java.util.List;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String token = null;

    @Before
    public void initLogin() {
        @NotNull final UserLoginRequest request = new UserLoginRequest("test", "test");
        @NotNull final UserLoginResponse response = authEndpoint.login(request);
        Assert.assertNotNull(response);
        token = response.getToken();
    }

    @Test
    public void testListProject() {
        @NotNull final ProjectListRequest requestOne = new ProjectListRequest(token, null);
        @NotNull List<ProjectDTO> projects = projectEndpoint.listProject(requestOne).getProjects();
        Assert.assertNotNull(projects);

        @NotNull final ProjectListRequest requestTwo = new ProjectListRequest(token, ProjectSort.BY_CREATED);
        projects = projectEndpoint.listProject(requestTwo).getProjects();
        Assert.assertNotNull(projects);

        @NotNull final ProjectListRequest requestThree = new ProjectListRequest(token, ProjectSort.BY_STATUS);
        projects = projectEndpoint.listProject(requestThree).getProjects();
        Assert.assertNotNull(projects);

        @NotNull final ProjectListRequest requestFour = new ProjectListRequest(token, ProjectSort.BY_NAME);
        projects = projectEndpoint.listProject(requestFour).getProjects();
        Assert.assertNotNull(projects);

        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listProject(new ProjectListRequest(null, null)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listProject(new ProjectListRequest()));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.listProject(null));
    }

    @Test
    public void testCreateProject() {
        @NotNull final String name = "newProject";
        @NotNull final String description = "newProject";
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(token, name, description);
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals(name, response.getProject().getName());
        Assert.assertEquals(description, response.getProject().getDescription());

        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createProject(null));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createProject(new ProjectCreateRequest(null, name, description)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createProject(new ProjectCreateRequest()));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createProject(new ProjectCreateRequest(token, null, description)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.createProject(new ProjectCreateRequest(token, name, null)));
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final String name = "Project";
        @NotNull final String description = "Project";
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(token, name, description);
        @NotNull ProjectCreateResponse createResponse = projectEndpoint.createProject(createRequest);
        @NotNull final String id = createResponse.getProject().getId();

        @NotNull final ProjectChangeStatusByIdRequest requestOne = new ProjectChangeStatusByIdRequest(token, id, Status.IN_PROGRESS);
        @NotNull ProjectChangeStatusByIdResponse responseOne = projectEndpoint.changeProjectStatusById(requestOne);
        Assert.assertEquals(Status.IN_PROGRESS, responseOne.getProject().getStatus());

        @NotNull final ProjectChangeStatusByIdRequest requestTwo = new ProjectChangeStatusByIdRequest(token, id, Status.COMPLETED);
        @NotNull ProjectChangeStatusByIdResponse responseTwo = projectEndpoint.changeProjectStatusById(requestTwo);
        Assert.assertEquals(Status.COMPLETED, responseTwo.getProject().getStatus());

        @NotNull final ProjectChangeStatusByIdRequest requestThree = new ProjectChangeStatusByIdRequest(token, id, Status.NON_STARTED);
        @NotNull ProjectChangeStatusByIdResponse responseThree = projectEndpoint.changeProjectStatusById(requestThree);
        Assert.assertEquals(Status.NON_STARTED, responseThree.getProject().getStatus());

        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusById(null));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(null, id, Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(token, null, Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(token, id, null)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest("a", id, Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(token, "a", Status.IN_PROGRESS)));
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @NotNull final ProjectChangeStatusByIndexRequest requestOne =
                new ProjectChangeStatusByIndexRequest(token, 0, Status.IN_PROGRESS);
        @NotNull ProjectChangeStatusByIndexResponse responseOne = projectEndpoint.changeProjectStatusByIndex(requestOne);
        Assert.assertEquals(Status.IN_PROGRESS, responseOne.getProject().getStatus());

        @NotNull final ProjectChangeStatusByIndexRequest requestTwo =
                new ProjectChangeStatusByIndexRequest(token, 0, Status.COMPLETED);
        @NotNull ProjectChangeStatusByIndexResponse responseTwo = projectEndpoint.changeProjectStatusByIndex(requestTwo);
        Assert.assertEquals(Status.COMPLETED, responseTwo.getProject().getStatus());

        @NotNull final ProjectChangeStatusByIndexRequest requestThree =
                new ProjectChangeStatusByIndexRequest(token, 0, Status.NON_STARTED);
        @NotNull ProjectChangeStatusByIndexResponse responseThree = projectEndpoint.changeProjectStatusByIndex(requestThree);
        Assert.assertEquals(Status.NON_STARTED, responseThree.getProject().getStatus());

        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusByIndex(null));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(null, 0, Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(token, 0, null)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.changeProjectStatusByIndex(
                        new ProjectChangeStatusByIndexRequest(token, 666, Status.IN_PROGRESS)));
    }

    @Test
    public void testUpdateProjectById() {
        @NotNull final String name = "Old";
        @NotNull final String description = "Old";
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(token, name, description);
        @NotNull final ProjectCreateResponse createResponse = projectEndpoint.createProject(createRequest);
        @NotNull final String id = createResponse.getProject().getId();

        @NotNull final String newName = "New";
        @NotNull final String newDescription = "New";
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(token, id, newName, newDescription);
        @NotNull ProjectUpdateByIdResponse response = projectEndpoint.updateProjectById(request);

        Assert.assertNotNull(response);
        Assert.assertNotEquals(name, response.getProject().getName());
        Assert.assertNotEquals(description, response.getProject().getDescription());

        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectById(null));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest("a", id, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(null, id, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(token, null, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(token, id, null, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(token, id, newName, null)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(token, "incorrect", newName, newDescription)));
    }

    @Test
    public void testUpdateProjectByIndex() {
        @NotNull final String name = "Old";
        @NotNull final String description = "Old";
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(token, name, description);
        projectEndpoint.createProject(createRequest);
        @NotNull final Integer index = projectEndpoint.listProject(
                new ProjectListRequest(token, null)).getProjects().size() - 1;

        @NotNull final String newName = "New";
        @NotNull final String newDescription = "New";
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(token, index, newName, newDescription);
        @NotNull ProjectUpdateByIndexResponse response = projectEndpoint.updateProjectByIndex(request);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(name, response.getProject().getName());
        Assert.assertNotEquals(description, response.getProject().getDescription());

        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectByIndex(null));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(null, index, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest("a", index, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(token, null, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(token, index, null, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(token, index, newName, null)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(token, 666, newName, description)));
    }

    @Test
    public void testRemoveProjectById() {
        @NotNull final String name = "ProjectForRemove";
        @NotNull final String description = "DescriptionForRemove";
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(token, name, description);
        @NotNull ProjectCreateResponse createResponse = projectEndpoint.createProject(createRequest);
        @NotNull final String id = createResponse.getProject().getId();

        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(token, id);
        @NotNull ProjectRemoveByIdResponse response = projectEndpoint.removeProjectById(request);
        Assert.assertNotNull(response);

        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.getProjectById(new ProjectGetByIdRequest(token, id)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(null, id)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, null)));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, "incorrect")));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest("incorrect", id)));
    }

    @Test
    public void testClearProject() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(token);
        @NotNull final ProjectClearResponse response = projectEndpoint.clearProject(request);
        Assert.assertNotNull(response);
        Assert.assertEquals(null, projectEndpoint.listProject(new ProjectListRequest(token, null)).getProjects());

        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearProject(null));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearProject(new ProjectClearRequest()));
        Assert.assertThrows(Exception.class, () ->
                projectEndpoint.clearProject(new ProjectClearRequest(null)));
    }

}
