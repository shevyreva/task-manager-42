package ru.t1.shevyreva.tm;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.shevyreva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.shevyreva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.shevyreva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.shevyreva.tm.dto.request.task.*;
import ru.t1.shevyreva.tm.dto.request.user.UserLoginRequest;
import ru.t1.shevyreva.tm.dto.response.task.*;
import ru.t1.shevyreva.tm.dto.response.user.UserLoginResponse;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.marker.SoapCategory;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @Nullable
    private String token = null;

    @Before
    public void initLogin() {
        @NotNull final UserLoginRequest request = new UserLoginRequest("test", "test");
        @NotNull final UserLoginResponse response = authEndpoint.login(request);
        Assert.assertNotNull(response);
        token = response.getToken();
    }

    @Test
    public void testListTask() {
        @NotNull final TaskListRequest requestOne = new TaskListRequest(token, null);
        @NotNull List<TaskDTO> tasks = taskEndpoint.listTask(requestOne).getTasks();
        Assert.assertNotNull(tasks);

        @NotNull final TaskListRequest requestTwo = new TaskListRequest(token, TaskSort.BY_CREATED);
        tasks = taskEndpoint.listTask(requestTwo).getTasks();
        Assert.assertNotNull(tasks);

        @NotNull final TaskListRequest requestThree = new TaskListRequest(token, TaskSort.BY_STATUS);
        tasks = taskEndpoint.listTask(requestThree).getTasks();
        Assert.assertNotNull(tasks);

        @NotNull final TaskListRequest requestFour = new TaskListRequest(token, TaskSort.BY_NAME);
        tasks = taskEndpoint.listTask(requestFour).getTasks();
        Assert.assertNotNull(tasks);

        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTask(new TaskListRequest(null, null)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTask(new TaskListRequest()));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.listTask(null));
    }

    @Test
    public void testCreateTask() {
        @NotNull final String name = "newTask";
        @NotNull final String description = "newTask";
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(token, name, description);
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(name, response.getTask().getName());
        Assert.assertEquals(description, response.getTask().getDescription());

        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTask(null));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTask(new TaskCreateRequest(null, name, description)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTask(new TaskCreateRequest()));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTask(new TaskCreateRequest(token, null, description)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.createTask(new TaskCreateRequest(token, name, null)));
    }

    @Test
    public void testChangeTaskStatusById() {
        @NotNull final String name = "Task";
        @NotNull final String description = "Task";
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token, name, description);
        @NotNull TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        @NotNull final String id = createResponse.getTask().getId();

        @NotNull final TaskChangeStatusByIdRequest requestOne = new TaskChangeStatusByIdRequest(token, id, Status.IN_PROGRESS);
        @NotNull TaskChangeStatusByIdResponse responseOne = taskEndpoint.changeTaskStatusById(requestOne);
        Assert.assertEquals(Status.IN_PROGRESS, responseOne.getTask().getStatus());

        @NotNull final TaskChangeStatusByIdRequest requestTwo = new TaskChangeStatusByIdRequest(token, id, Status.COMPLETED);
        @NotNull TaskChangeStatusByIdResponse responseTwo = taskEndpoint.changeTaskStatusById(requestTwo);
        Assert.assertEquals(Status.COMPLETED, responseTwo.getTask().getStatus());

        @NotNull final TaskChangeStatusByIdRequest requestThree = new TaskChangeStatusByIdRequest(token, id, Status.NON_STARTED);
        @NotNull TaskChangeStatusByIdResponse responseThree = taskEndpoint.changeTaskStatusById(requestThree);
        Assert.assertEquals(Status.NON_STARTED, responseThree.getTask().getStatus());

        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusById(null));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(null, id, Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(token, null, Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(token, id, null)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest("a", id, Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(token, "a", Status.IN_PROGRESS)));
    }

    @Test
    public void testChangeTaskStatusByIndex() {
        @NotNull final TaskChangeStatusByIndexRequest requestOne =
                new TaskChangeStatusByIndexRequest(token, 0, Status.IN_PROGRESS);
        @NotNull TaskChangeStatusByIndexResponse responseOne = taskEndpoint.changeTaskStatusByIndex(requestOne);
        Assert.assertEquals(Status.IN_PROGRESS, responseOne.getTask().getStatus());

        @NotNull final TaskChangeStatusByIndexRequest requestTwo =
                new TaskChangeStatusByIndexRequest(token, 0, Status.COMPLETED);
        @NotNull TaskChangeStatusByIndexResponse responseTwo = taskEndpoint.changeTaskStatusByIndex(requestTwo);
        Assert.assertEquals(Status.COMPLETED, responseTwo.getTask().getStatus());

        @NotNull final TaskChangeStatusByIndexRequest requestThree =
                new TaskChangeStatusByIndexRequest(token, 0, Status.NON_STARTED);
        @NotNull TaskChangeStatusByIndexResponse responseThree = taskEndpoint.changeTaskStatusByIndex(requestThree);
        Assert.assertEquals(Status.NON_STARTED, responseThree.getTask().getStatus());

        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIndex(null));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(null, 0, Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(token, 0, null)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.changeTaskStatusByIndex(
                        new TaskChangeStatusByIndexRequest(token, 666, Status.IN_PROGRESS)));
    }

    @Test
    public void testUpdateTaskById() {
        @NotNull final String name = "Old";
        @NotNull final String description = "Old";
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token, name, description);
        @NotNull final TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        @NotNull final String id = createResponse.getTask().getId();

        @NotNull final String newName = "New";
        @NotNull final String newDescription = "New";
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(token, id, newName, newDescription);
        @NotNull TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(request);

        Assert.assertNotNull(response);
        Assert.assertNotEquals(name, response.getTask().getName());
        Assert.assertNotEquals(description, response.getTask().getDescription());

        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskById(null));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskById(new TaskUpdateByIdRequest("a", id, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(null, id, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(token, null, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(token, id, null, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(token, id, newName, null)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(token, "incorrect", newName, newDescription)));
    }

    @Test
    public void testUpdateTaskByIndex() {
        @NotNull final String name = "Old";
        @NotNull final String description = "Old";
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token, name, description);
        taskEndpoint.createTask(createRequest);
        @NotNull final Integer index = taskEndpoint.listTask(
                new TaskListRequest(token, null)).getTasks().size() - 1;

        @NotNull final String newName = "New";
        @NotNull final String newDescription = "New";
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(token, index, newName, newDescription);
        @NotNull TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndex(request);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(name, response.getTask().getName());
        Assert.assertNotEquals(description, response.getTask().getDescription());

        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndex(null));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest(null, index, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest("a", index, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest(token, null, newName, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest(token, index, null, newDescription)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest(token, index, newName, null)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest(token, 666, newName, description)));
    }

    @Test
    public void testRemoveTaskById() {
        @NotNull final String name = "TaskForRemove";
        @NotNull final String description = "DescriptionForRemove";
        @NotNull final TaskCreateRequest createRequest = new TaskCreateRequest(token, name, description);
        @NotNull TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        @NotNull final String id = createResponse.getTask().getId();

        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(token, id);
        @NotNull TaskRemoveByIdResponse response = taskEndpoint.removeTaskById(request);
        Assert.assertNotNull(response);

        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.getTaskById(new TaskGetByIdRequest(token, id)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(null, id)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, null)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, "incorrect")));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.removeTaskById(new TaskRemoveByIdRequest("incorrect", id)));
    }

    @Test
    public void testClearTask() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(token);
        @NotNull final TaskClearResponse response = taskEndpoint.clearTask(request);
        Assert.assertNotNull(response);
        Assert.assertEquals(null, taskEndpoint.listTask(new TaskListRequest(token, null)).getTasks());

        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTask(null));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTask(new TaskClearRequest()));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.clearTask(new TaskClearRequest(null)));
    }

    @Test
    public void testBindToProject() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(token, "abc", "abc");
        @NotNull ProjectDTO project = projectEndpoint.createProject(projectRequest).getProject();
        @NotNull final String projectId = project.getId();

        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(token, "abc", "abc");
        @NotNull final TaskDTO task = taskEndpoint.createTask(taskRequest).getTask();
        @NotNull final String taskId = task.getId();

        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(token, taskId, projectId);
        @NotNull final TaskBindToProjectResponse response = taskEndpoint.bindTaskToProject(request);
        Assert.assertNotNull(response);

        @NotNull final TaskDTO bindedTask = taskEndpoint.getTaskById(new TaskGetByIdRequest(token, taskId)).getTask();
        @NotNull final String bindProjectId = bindedTask.getProjectId();
        Assert.assertNotNull(bindProjectId);
        Assert.assertEquals(projectId, bindProjectId);

        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProject(null));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(null, taskId, projectId)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, null, projectId)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, taskId, null)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest("a", taskId, projectId)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, "a", projectId)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProject(new TaskBindToProjectRequest(token, taskId, "a")));
    }

    @Test
    public void testUnbindFromProject() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(token, "abc", "abc");
        @NotNull ProjectDTO project = projectEndpoint.createProject(projectRequest).getProject();
        @NotNull final String projectId = project.getId();

        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(token, "abc", "abc");
        @NotNull final TaskDTO task = taskEndpoint.createTask(taskRequest).getTask();
        @NotNull final String taskId = task.getId();
        task.setProjectId(projectId);

        @NotNull final TaskUnbindToProjectRequest request = new TaskUnbindToProjectRequest(token, taskId, projectId);
        @NotNull final TaskUnbindToProjectResponse response = taskEndpoint.unbindTaskFromProject(request);
        Assert.assertNotNull(response);

        @NotNull final TaskDTO bindedTask = taskEndpoint.getTaskById(new TaskGetByIdRequest(token, taskId)).getTask();
        @NotNull final String unbindProjectId = bindedTask.getProjectId();
        Assert.assertNull(unbindProjectId);
        Assert.assertNotEquals(projectId, unbindProjectId);

        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.bindTaskToProject(null));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProject(new TaskUnbindToProjectRequest(null, taskId, projectId)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProject(new TaskUnbindToProjectRequest(token, null, projectId)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProject(new TaskUnbindToProjectRequest(token, taskId, null)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProject(new TaskUnbindToProjectRequest("a", taskId, projectId)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProject(new TaskUnbindToProjectRequest(token, "a", projectId)));
        Assert.assertThrows(Exception.class, () ->
                taskEndpoint.unbindTaskFromProject(new TaskUnbindToProjectRequest(token, taskId, "a")));
    }

}
