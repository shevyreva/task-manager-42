package ru.t1.shevyreva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.data.DataSaveXmlJaxBRequest;
import ru.t1.shevyreva.tm.enumerated.Role;

public class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save Data to xml file.";

    @NotNull
    private final String NAME = "data-save-xml-jaxb";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataSaveXmlJaxBRequest request = new DataSaveXmlJaxBRequest(getToken());
        getDomainEndpoint().saveXmlDataJaxB(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
