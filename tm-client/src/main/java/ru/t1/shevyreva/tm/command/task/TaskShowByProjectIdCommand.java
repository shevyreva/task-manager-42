package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.dto.request.task.TaskListByProjectIdRequest;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Show tasks by project id.";

    @NotNull
    private final String NAME = "task-show-by-project-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW ALL TASKS BY PROJECT ID]");
        System.out.println("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken(), projectId);
        @NotNull final List<TaskDTO> tasks = getTaskEndpoint().listTaskByProjectId(request).getTasks();
        int index = 1;
        for (final TaskDTO task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
