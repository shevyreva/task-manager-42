package ru.t1.shevyreva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.dto.request.user.UserProfileRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "View user profile.";

    @NotNull
    private final String NAME = "user-profile";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @NotNull final UserDTO user = getAuthEndpoint().profile(request).getUser();
        System.out.println("[USER PROFILE]");
        System.out.println("[USER ID]: " + user.getId());
        System.out.println("[LOGIN]: " + user.getLogin());
        System.out.println("[EMAIL]: " + user.getEmail());
        System.out.println("[FIRST NAME]: " + user.getFirstName());
        System.out.println("[LAST NAME]: " + user.getLastName());
        System.out.println("[MIDDLE NAME]: " + user.getMiddleName());
        System.out.println("[ROLE]: " + user.getRole());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
