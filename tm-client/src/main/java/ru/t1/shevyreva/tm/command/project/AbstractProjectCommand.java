package ru.t1.shevyreva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    public IProjectEndpoint getProjectEndpoint() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getProjectEndpoint();
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[PROJECT ID]: " + project.getId());
        System.out.println("[PROJECT NAME]: " + project.getName());
        System.out.println("[PROJECT DESCRIPTION]: " + project.getDescription());
        System.out.println("[PROJECT STATUS]: " + Status.toName(project.getStatus()));
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
