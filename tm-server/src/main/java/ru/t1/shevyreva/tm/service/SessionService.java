package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.ISessionRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.ISessionService;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;

import java.util.Collection;
import java.util.List;

public class SessionService implements ISessionService {

    private final IConnectionService connectionService;

    public SessionService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @SneakyThrows
    public SessionDTO add(
            @Nullable final String userId,
            @Nullable final SessionDTO session
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        session.setUserId(userId);

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(session);
            sqlSession.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public SessionDTO add(
            @Nullable final SessionDTO session
    ) {
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            add(session.getUserId(), session);
            sqlSession.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final SessionDTO session = findOneById(userId, id);
            if (session == null) throw new TaskNotFoundException();
            return session;
        }
    }

    @NotNull
    @SneakyThrows
    public SessionDTO removeOne(@Nullable final SessionDTO session) {
        if (session == null) throw new ModelNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeOne(session);
            sqlSession.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public SessionDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            SessionDTO session = findOneById(userId, id);
            sessionRepository.removeOneById(userId, id);
            sqlSession.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public List<SessionDTO> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAll();
        }
    }

    @NotNull
    @SneakyThrows
    public Collection<SessionDTO> add(@NotNull final Collection<SessionDTO> models) {
        if (models == null) throw new ModelNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            for (@NotNull SessionDTO session : models) {
                sessionRepository.add(session);
            }
            sqlSession.commit();
            return models;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @NotNull
    @SneakyThrows
    public Collection<SessionDTO> set(@NotNull final Collection<SessionDTO> models) {
        @Nullable final Collection<SessionDTO> entities;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            removeAll();
            entities = add(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return entities;
    }

    public boolean existsById(@NotNull final String id) {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection();) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return (sessionRepository.findOneById(id) != null);
        }
    }

}
