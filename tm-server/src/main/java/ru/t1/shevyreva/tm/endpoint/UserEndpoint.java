package ru.t1.shevyreva.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.IUserEndpoint;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.dto.request.user.*;
import ru.t1.shevyreva.tm.dto.response.user.*;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.model.Session;
import ru.t1.shevyreva.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.shevyreva.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Override
    @SneakyThrows
    @WebMethod
    public @NotNull UserRegistryResponse registry(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegistryRequest request
    ) {
        @Nullable String login = request.getLogin();
        @Nullable String password = request.getPassword();
        @Nullable String email = request.getEmail();
        @NotNull UserDTO user = getUserService().create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @Override
    @WebMethod
    public @NotNull UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable String password = request.getPassword();
        @NotNull UserDTO user = getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @Override
    @WebMethod
    public @NotNull UserUpdateProfileResponse updateProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable String firstName = request.getFirstName();
        @Nullable String lastName = request.getLastName();
        @Nullable String middleName = request.getMiddleName();
        @NotNull UserDTO user = getUserService().updateUser(userId, firstName, middleName, lastName);
        return new UserUpdateProfileResponse(user);
    }

    @Override
    @WebMethod
    public @NotNull UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final UserDTO user = getUserService().lockUser(login);
        return new UserLockResponse(user);
    }

    @Override
    @WebMethod
    public @NotNull UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final UserDTO user = getUserService().unlockUser(login);
        return new UserUnlockResponse(user);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final String email = request.getEmail();
        @Nullable final String id = request.getUserId();
        @Nullable final UserDTO user;
        if (login != null) user = getUserService().removeByLogin(login);
        else if (email != null) user = getUserService().removeByEmail(email);
        else if (id != null) user = getUserService().removeOneById(id);
        else return new UserRemoveResponse(null);
        return new UserRemoveResponse(user);
    }

}
