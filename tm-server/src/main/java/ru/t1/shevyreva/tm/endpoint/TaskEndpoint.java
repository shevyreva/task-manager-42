package ru.t1.shevyreva.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.shevyreva.tm.api.service.IProjectTaskService;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.api.service.ITaskService;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.dto.request.task.*;
import ru.t1.shevyreva.tm.dto.response.task.*;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.model.Session;
import ru.t1.shevyreva.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.shevyreva.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    @WebMethod
    public @NotNull TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable TaskDTO task = getTaskService().changeTaskStatusById(userId, taskId, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable TaskDTO task = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        getTaskService().removeAll(userId);
        return new TaskClearResponse();
    }

    @Override
    @WebMethod
    public @NotNull TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final TaskDTO task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull TaskGetByIdResponse getTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String taskId = request.getId();
        @Nullable TaskDTO task = getTaskService().findOneById(userId, taskId);
        return new TaskGetByIdResponse(task);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull TaskGetByIndexResponse getTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskGetByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable TaskDTO task = getTaskService().findOneByIndex(userId, index);
        return new TaskGetByIndexResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final TaskSort sort = request.getSort();
        @Nullable final List<TaskDTO> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String taskId = request.getId();
        @NotNull final TaskDTO task = getTaskService().removeOneById(userId, taskId);
        return new TaskRemoveByIdResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIndexRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

    @Override
    @WebMethod
    public @NotNull TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @Override
    @WebMethod
    public @NotNull TaskUnbindToProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindToProjectRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskToProject(userId, projectId, taskId);
        return new TaskUnbindToProjectResponse();
    }

    @Override
    @WebMethod
    public @NotNull TaskRemoveByProjectIdResponse removeTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByProjectIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        getProjectTaskService().removeByProjectId(userId, projectId);
        return new TaskRemoveByProjectIdResponse();
    }

    @Override
    @WebMethod
    public @NotNull TaskLisByProjectIdResponse listTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListByProjectIdRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<TaskDTO> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskLisByProjectIdResponse(tasks);
    }

}
