package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;
import ru.t1.shevyreva.tm.exception.field.EmailEmptyException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.LoginEmptyException;
import ru.t1.shevyreva.tm.exception.field.PasswordEmptyException;
import ru.t1.shevyreva.tm.exception.user.ExistsEmailException;
import ru.t1.shevyreva.tm.exception.user.ExistsLoginException;
import ru.t1.shevyreva.tm.exception.user.RoleEmptyException;
import ru.t1.shevyreva.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

public class UserService implements IUserService {

    private final IConnectionService connectionService;

    private final IPropertyService propertyService;

    public UserService(@NotNull IConnectionService connectionService, @NotNull IPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(Role.USUAL);

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(Role.USUAL);
        user.setEmail(email);

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password, propertyService));
        user.setRole(role);

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByLogin(login);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByEmail(email);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final UserDTO user = userRepository.findByLogin(login);
            userRepository.removeOne(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final UserDTO user = userRepository.findByEmail(email);
            userRepository.removeOne(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final UserDTO user = userRepository.findOneById(id);
            userRepository.removeOne(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final UserDTO user = findOneById(id);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            userRepository.update(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final UserDTO user = findOneById(id);
            user.setPasswordHash(HashUtil.salt(password, propertyService));
            userRepository.update(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return (userRepository.findByLogin(login) != null);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return (userRepository.findByEmail(email) != null);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public UserDTO lockUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull UserDTO user = findByLogin(login);
            user.setLocked(true);
            userRepository.update(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public UserDTO unlockUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull UserDTO user = findByLogin(login);
            user.setLocked(false);
            userRepository.update(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection();) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final UserDTO user = userRepository.findOneById(id);
            if (user == null) throw new ModelNotFoundException();
            return user;
        }
    }

    @SneakyThrows
    public List<UserDTO> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findAll();
        }
    }

    @NotNull
    @SneakyThrows
    public Collection<UserDTO> add(@NotNull final Collection<UserDTO> models) {
        if (models == null) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            for (@NotNull UserDTO user : models) {
                userRepository.add(user);
            }
            sqlSession.commit();
            return models;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @NotNull
    @SneakyThrows
    public Collection<UserDTO> set(@NotNull final Collection<UserDTO> models) {
        @Nullable final Collection<UserDTO> entities;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            removeAll();
            entities = add(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return entities;
    }

}
