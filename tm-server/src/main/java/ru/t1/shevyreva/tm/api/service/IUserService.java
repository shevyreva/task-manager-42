package ru.t1.shevyreva.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.enumerated.Role;


import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    @SneakyThrows
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    @SneakyThrows
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    @SneakyThrows
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    @SneakyThrows
    UserDTO findByLogin(@Nullable String login);

    @NotNull
    @SneakyThrows
    UserDTO findOneById(@Nullable final String id);

    @NotNull
    @SneakyThrows
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    @SneakyThrows
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    @SneakyThrows
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    @SneakyThrows
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String middleName, @Nullable String lastName);

    @NotNull
    @SneakyThrows
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    @NotNull
    @SneakyThrows
    UserDTO lockUser(@NotNull final String login);

    @NotNull
    @SneakyThrows
    UserDTO unlockUser(@NotNull final String login);

    @NotNull
    @SneakyThrows
    Collection<UserDTO> set(@NotNull final Collection<UserDTO> models);

    @SneakyThrows
    void removeAll();

    @NotNull
    @SneakyThrows
    Collection<UserDTO> add(@NotNull final Collection<UserDTO> models);

    @SneakyThrows
    List<UserDTO> findAll();

    @NotNull
    @SneakyThrows
    UserDTO removeOneById(@Nullable final String id);

}
