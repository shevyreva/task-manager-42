package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.ITaskService;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.*;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public final class TaskService implements ITaskService {

    private final IConnectionService connectionService;

    public TaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        try {
            taskRepository.addWithUserId(userId, task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        try {
            taskRepository.addWithUserId(userId, task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllByProjectId(userId, projectId);
        }
    }

    @NotNull
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final TaskSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (sort == null) return taskRepository.findAllWithUser(userId);
            if (sort.getDisplayName() == "BY_CREATED")
                return taskRepository.findAllOrderByCreated(userId);
            else if (sort.getDisplayName() == "BY_NAME")
                return taskRepository.findAllOrderByName(userId);
            else return null;
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final TaskDTO task = taskRepository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();

        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final TaskDTO task = taskRepository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @NotNull
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.getSize(userId);
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO removeOne(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new ModelNotFoundException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeOne(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            TaskDTO task = taskRepository.findOneById(userId, id);
            taskRepository.removeOneById(userId, id);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public List<TaskDTO> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAll();
        }
    }

    @NotNull
    @SneakyThrows
    public Collection<TaskDTO> add(@NotNull final Collection<TaskDTO> models) {
        if (models == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            for (@NotNull TaskDTO task : models) {
                taskRepository.add(task);
            }
            sqlSession.commit();
            return models;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @SneakyThrows
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @NotNull
    @SneakyThrows
    public Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> models) {
        @Nullable final Collection<TaskDTO> entities;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            removeAll();
            entities = add(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return entities;
    }

    public boolean existsById(@NotNull final String user_id, @NotNull final String id) {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection();) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return (taskRepository.findOneById(user_id, id) != null);
        }
    }

}
