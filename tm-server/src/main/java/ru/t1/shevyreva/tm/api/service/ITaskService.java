package ru.t1.shevyreva.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    List<TaskDTO> findAll(@Nullable String userId, @Nullable TaskSort sort);

    @NotNull
    int getSize(@Nullable final String userId);

    @SneakyThrows
    void removeAll(@Nullable final String userId);

    @NotNull
    @SneakyThrows
    TaskDTO removeOneById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    @SneakyThrows
    TaskDTO removeOne(@Nullable final String userId, @Nullable final TaskDTO task);

    @NotNull
    @SneakyThrows
    TaskDTO findOneById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    @SneakyThrows
    TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    @NotNull
    @SneakyThrows
    Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> models);

    @SneakyThrows
    void removeAll();

    @NotNull
    @SneakyThrows
    Collection<TaskDTO> add(@NotNull final Collection<TaskDTO> models);

    @SneakyThrows
    List<TaskDTO> findAll();

    boolean existsById(@NotNull final String user_id, @NotNull final String id);

}
