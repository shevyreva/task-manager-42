package ru.t1.shevyreva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password, role, created, first_name, last_name, middle_name, locked, email) " +
            "VALUES (#{id},#{login},#{password},#{role},#{created}, #{firstName}, #{lastName}, #{middleName}, #{locked}, #{email});")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password")
    })
    void add(@NotNull UserDTO user);

    @Delete("DELETE FROM tm_user")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password")
    })
    @Nullable
    List<UserDTO> findAll();

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password")
    })
    @Nullable
    UserDTO findOneById(@NotNull @Param("id") String id);

    @Update("UPDATE tm_user SET first_name = #{firstName}, last_name = #{lastName}, middle_name = #{middleName}, " +
            "login = #{login}, email = #{email}, " +
            "role = #{role}, password = #{password}, locked = #{locked} " +
            "WHERE id = #{id}")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password")
    })
    void update(@NotNull final UserDTO user);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeOne(@NotNull UserDTO user);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeOneById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password")
    })
    UserDTO findByLogin(@Param("login") @NotNull String login);

    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password")
    })
    UserDTO findByEmail(@Param("email") @NotNull String email);

    @Delete("TRUNCATE TABLE tm_user;")
    void removeAll();

}
