package ru.t1.shevyreva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.*;
import ru.t1.shevyreva.tm.api.service.*;
import ru.t1.shevyreva.tm.endpoint.*;
import ru.t1.shevyreva.tm.service.*;
import ru.t1.shevyreva.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;


@Getter
@Setter
@NoArgsConstructor
public class Bootstrap implements IServiceLocator {
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);
    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);
    @NotNull
    private final Backup backup = new Backup(this);
    @NotNull
    private final String PACKAGE_COMMANDS = "ru.t1.shevyreva.tm.command";
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);
    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    {
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(authEndpoint);
        registry(domainEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        initPID();
        loggerService.info("**Welcome to Task Manager Server**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("**Shutdown Task Manager Server**");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull File file = new File(fileName);
        file.deleteOnExit();
    }

}
