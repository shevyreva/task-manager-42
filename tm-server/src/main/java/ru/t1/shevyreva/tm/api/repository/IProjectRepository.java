package ru.t1.shevyreva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, name, description, status, created) " +
            "VALUES (#{id},#{name},#{description},#{status},#{created});")
    void add(@NotNull ProjectDTO project);

    @Insert("INSERT INTO tm_project (id, user_id, name, description, status, created " +
            "VALUES (#{id},#{userId},#{name},#{description},#{status},#{created};")
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId};")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAllWithUser(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAll();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    ProjectDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    ProjectDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT count(*) FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    int getSize(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_project SET user_id = #{userId}, name = #{name}, description = #{description}," +
            " status =#{status} " +
            "WHERE id = #{id}")
    void update(@NotNull final ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    void removeOne(@NotNull ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    void removeOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("TRUNCATE TABLE tm_project;")
    void removeAll();

    @Select("SELECT COUNT(1) = 1 FROM tm_project WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

}
