package ru.t1.shevyreva.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    @SneakyThrows
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    @SneakyThrows
    ProjectDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer Index, @Nullable Status status);

    @NotNull
    @SneakyThrows
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    @SneakyThrows
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    @NotNull
    @SneakyThrows
    ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    @SneakyThrows
    ProjectDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    int getSize(@Nullable final String userId);

    @NotNull
    @SneakyThrows
    ProjectDTO removeOne(@Nullable final String userId, @Nullable final ProjectDTO project);

    @NotNull
    @SneakyThrows
    ProjectDTO removeOneById(@Nullable final String userId, @Nullable final String id);

    void removeAll(@Nullable final String userId);

    @NotNull
    @SneakyThrows
    Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> models);

    @SneakyThrows
    void removeAll();

    @NotNull
    @SneakyThrows
    Collection<ProjectDTO> add(@NotNull final Collection<ProjectDTO> models);

    @SneakyThrows
    List<ProjectDTO> findAll();

    boolean existsById(@NotNull final String user_id, @NotNull final String id);

}
