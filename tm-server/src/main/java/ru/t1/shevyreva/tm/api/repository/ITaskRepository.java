package ru.t1.shevyreva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, name, description, status, created, project_id) " +
            "VALUES (#{id},#{name},#{description},#{status},#{created},#{projectId});")
    void add(@NotNull TaskDTO task);

    @Insert("INSERT INTO tm_task (id, user_id, name, description, status, created, project_id) " +
            "VALUES (#{id},#{userId},#{name},#{description},#{status},#{created},#{projectId});")
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull TaskDTO task);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    List<TaskDTO> findAllWithUser(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<TaskDTO> findAll();

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    List<TaskDTO> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    List<TaskDTO> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    TaskDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_Id")
    })
    @Nullable
    TaskDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT count(*) FROM tm_task WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_task SET user_id = #{userId}, project_id = #{projectId}, name = #{name}, description = #{description}," +
            " status =#{status} " +
            "WHERE id = #{id}")
    void update(@NotNull final TaskDTO task);

    @Delete("DELETE FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    void removeOne(@NotNull TaskDTO model);

    @Delete("DELETE FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    void removeOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("TRUNCATE TABLE tm_task;")
    void removeAll();

    @Select("SELECT COUNT(1) = 1 FROM tm_task WHERE id = #{id} AND user_id = #{userId}")
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

}
